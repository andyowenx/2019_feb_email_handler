#insert into email_pw (account, sever, password) values ('','','');
import mysql.connector
from mysql.connector import errorcode
import os
debug_level = 2

def close_db(db_data):
    db_data['conn'].close()
    db_data['cursor'].close()

def isLegalRow(account, sever, password):
    if len(account) > 30 or len(sever) > 30 or len(password) > 30:
        return False
    return True

def read_data(config,db_data):
    db_table =  config['mysql_table']
    count = 0
    for filename in config['filename']:
        fd = open(filename, 'r')
        for content in fd.readlines():
            #remove \r\n in the end
            content = content[:-2]

            #There is a backslash inside. Skip this row.
            if content.find("\\") != -1:
                continue

            # content[0] = email address, content[1] = password
            content = content.split(':')
            email = content[0].split('@')
            email_account = email[0]
            email_sever = email[1]
            email_password = content[1]

            #data too long, skip it.
            if isLegalRow(email_account, email_sever, email_password) is False:
                continue

            command = "insert into " + str(db_table) + " (account, sever, password) values (\'" + str(email_account) + "\',\'" + str(email_sever) + "\',\'" + str(email_password) + "\');"

            if debug_level >= 3 :
                print "[read_data]: command = "+str(command)
            
            db_data['cursor'].execute(command)

            count+=1
            if count % 10000 == 0:
                db_data['conn'].commit()
                if debug_level >= 2:
                    print "[read_data][Info] Insert "+str(count)+" rows."

        db_data['conn'].commit()
        if debug_level >= 2:
            print "[read_data][Info] Insert "+str(count)+" rows."
    

def link_db(config, db_data):
    try:
        db_data['conn'] = mysql.connector.connect(user = config['mysql_user'], password = config['mysql_password'],
                                       host = config['mysql_host'], database = config['mysql_database'])
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR and debug_level >=1:
            print "[link_db]: mysql username or password is wrong."
            exit(-1)
        elif err.errno == errorcode.ER_BAD_DB_ERROR and debug_level >=1:
            print "[link_db]: mysql database is not exist."
            exit(-1)
        elif debug_level>= 1:
            print "[link_db]: " + str(err)
            exit(-1)
    
    db_data['cursor'] = db_data['conn'].cursor()

def open_folder(config, path):
    for root, folder, files in os.walk(path):
        for filename in files:
            config['filename'].append(str(root)+"/"+str(filename))
        for folder_name in folder:
            open_folder(config, str(root)+"/"+str(folder_name))


def read_config(config):


    config['filename'] = []
    #default value for config file
    config['mysql_user'] = ""
    config['mysql_password'] = ""
    config['mysql_host'] = ""
    config['mysql_database'] = ""

    #read config file
    fd = open(config['config_filename'], 'r')

    for command in fd.readlines():
        if command[0] == '#':
            continue
        #remove the '\n' in the end
        command = command[:-1]
        command = command.split(' ')
        if command[0] == 'filename' :
            config['filename'].append(command[1])
        elif command[0] == 'folder': # this will recursive read all the files in the folder
            open_folder(config, command[1])
        elif command[0] == 'mysql_user':
            config['mysql_user'] = command[1]
        elif command[0] == 'mysql_password':
            if len(command) == 1: #no password
                config['mysql_password'] = ''
            else:
                config['mysql_password'] = command[1]
        elif command[0] == 'mysql_host':
            config['mysql_host'] = command[1]
        elif command[0] == 'mysql_database':
            config['mysql_database'] = command[1]
        elif command[0] == 'mysql_table':
            config['mysql_table'] = command[1]

    fd.close()

    if debug_level >= 3:
        print "[read_config][debug]: config = " + str(config)

def main():

    config = {}
    db_data={}
    config['config_filename'] = './config'
    read_config(config)
    link_db(config, db_data)
    read_data(config,db_data)
    close_db(db_data)
    

if __name__ == '__main__':
    main()
