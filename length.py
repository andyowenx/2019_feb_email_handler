import os
debug_level =0

def read_data(config,db_data):

    fd_account = open("./account","w")
    fd_sever = open("./sever","w")
    fd_password = open("./password", "w")
    count = 0
    account_dict={}
    account_dict['30']=0
    account_dict['40']=0
    account_dict['40_more']=0
    sever_dict={}
    sever_dict['30']=0
    sever_dict['40']=0
    sever_dict['40_more']=0
    password_dict={}
    password_dict['30']=0
    password_dict['40']=0
    password_dict['40_more']=0
    for filename in config['filename']:
        fd = open(filename, 'r')
        for content in fd.readlines():
            #remove \r\n in the end
            content = content[:-2]
            #remove special char
            orig_content = content+"\n"
            content = content.replace("\\","")

            # content[0] = email address, content[1] = password
            content = content.split(':')
            email = content[0].split('@')
            email_account = email[0]
            email_sever = email[1]
            email_password = content[1]
            count +=1

            if len(email_account) <= 30:
                account_dict['30']+=1
            elif len(email_account) <= 40:
                account_dict['40']+=1
                fd_account.write(orig_content)
            else:
                account_dict['40_more']+=1
                fd_account.write(orig_content)

            
            if len(email_sever) <= 30:
                sever_dict['30']+=1
            elif len(email_sever) <= 40:
                sever_dict['40']+=1
                fd_sever.write(orig_content)
            else:
                sever_dict['40_more']+=1
                fd_sever.write(orig_content)

            if len(email_password) <= 30:
                password_dict['30']+=1
            elif len(email_password) <= 40:
                password_dict['40']+=1
                fd_password.write(orig_content)
            else:
                password_dict['40_more']+=1
                fd_password.write(orig_content)


            if count % 1000000 == 0:
                print "scan "+str(count)+" rows."
    
    print "Account:"
    print "0 ~  30:\t"+str(account_dict['30'])
    print "31 ~ 40:\t"+str(account_dict['40'])
    print " > 40:\t"+str(account_dict['40_more'])
    print "--------------------------------------------"
    print "Sever:"
    print "0 ~  30:\t"+str(sever_dict['30'])
    print "31 ~ 40:\t"+str(sever_dict['40'])
    print " > 40:\t"+str(sever_dict['40_more'])
    print "--------------------------------------------"
    print "Password:"
    print "0 ~  30:\t"+str(password_dict['30'])
    print "31 ~ 40:\t"+str(password_dict['40'])
    print " > 40:\t"+str(password_dict['40_more'])

def link_db(config, db_data):
    try:
        db_data['conn'] = mysql.connector.connect(user = config['mysql_user'], password = config['mysql_password'],
                                       host = config['mysql_host'], database = config['mysql_database'])
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR and debug_level >=1:
            print "[link_db]: mysql username or password is wrong."
            exit(-1)
        elif err.errno == errorcode.ER_BAD_DB_ERROR and debug_level >=1:
            print "[link_db]: mysql database is not exist."
            exit(-1)
        elif debug_level>= 1:
            print "[link_db]: " + str(err)
            exit(-1)
    
    db_data['cursor'] = db_data['conn'].cursor()

def open_folder(config, path):
    for root, folder, files in os.walk(path):
        for filename in files:
            config['filename'].append(str(root)+"/"+str(filename))
        for folder_name in folder:
            open_folder(config, str(root)+"/"+str(folder_name))


def read_config(config):


    config['filename'] = []
    #default value for config file
    config['mysql_user'] = ""
    config['mysql_password'] = ""
    config['mysql_host'] = ""
    config['mysql_database'] = ""

    #read config file
    fd = open(config['config_filename'], 'r')

    for command in fd.readlines():
        if command[0] == '#':
            continue
        #remove the '\n' in the end
        command = command[:-1]
        command = command.split(' ')
        if command[0] == 'filename' :
            config['filename'].append(command[1])
        elif command[0] == 'folder': # this will recursive read all the files in the folder
            open_folder(config, command[1])
        elif command[0] == 'mysql_user':
            config['mysql_user'] = command[1]
        elif command[0] == 'mysql_password':
            if len(command) == 1: #no password
                config['mysql_password'] = ''
            else:
                config['mysql_password'] = command[1]
        elif command[0] == 'mysql_host':
            config['mysql_host'] = command[1]
        elif command[0] == 'mysql_database':
            config['mysql_database'] = command[1]
        elif command[0] == 'mysql_table':
            config['mysql_table'] = command[1]

    fd.close()

    if debug_level >= 3:
        print "[read_config][debug]: config = " + str(config)

def main():

    config = {}
    db_data={}
    config['config_filename'] = './config'
    read_config(config)
    #link_db(config, db_data)
    read_data(config,db_data)
    #close_db(db_data)
    

if __name__ == '__main__':
    main()
